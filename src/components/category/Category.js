import React from "react";
import { Link } from "wouter";

export default function Category({ name, options = [] }) {
  return (
    <section className="category" >
      <h3 category-title>
        {name}
      </h3>
      <ul className="catefory-list">
        {options.map((singleOption) => (
          <li
            key={singleOption}
            type='primary'>
            <Link to={`/search/${singleOption}`} className="category-Link" >
              {singleOption}
            </Link>
          </li>
        ))}
      </ul>
    </section>
  )
};
