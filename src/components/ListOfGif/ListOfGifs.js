import React from 'react';
import Gif from 'components/gif/Gif';
import './listOfGifs.css';

export default function ListOfGifs({ gifs }) {
  return <div className='ListOfGifs'>
    {
      gifs.map(({ id, title, url }) =>
        <Gif
          id={id}
          key={id}
          title={title}
          url={url}
        />
      )
    }
  </div>
}
