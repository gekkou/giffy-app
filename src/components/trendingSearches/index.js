import React, { Suspense } from "react";
import useNearScreen from "hooks/useNearScreen";

const TrendingSearches = React.lazy(
  () => import('./TrendingSearches.js')
)

export default function LazyTrending() {
  const { isNearScreen, fromRef } = useNearScreen({ distance: '200px' })

  return <div ref={fromRef}>
    <Suspense fallback={'Estoy Cargando...'}>
      {isNearScreen ? <TrendingSearches /> : null}
    </Suspense>
  </div>
}
