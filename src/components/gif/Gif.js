import React from "react";
import { Link } from "wouter";
import './Gifs.css';

export default function Gif({ title, id, url }) {
  return (
    <div className="Gif">
      <Link
        to={`/gif/${id}`}
        className="gif-link">
        {/* 
        <h4>{title}</h4>
*/}
        <img
          loading="lazy"
          src={url}
          alt={title}
        />
      </Link>
    </div>
  )
}
