import React from 'react';
import ListOfGifs from 'components/ListOfGif/ListOfGifs';
import { useGifs } from 'hooks/useGifs/useGifs';

export default function SearchResults({ params }) {
  const { keyword } = params;
  const { loading, gifs, setPage } = useGifs({ keyword });

  const handleNextPage = () => setPage(prevPage => prevPage + 1)


  return <>
    {
      loading
        ?
        <i>Loading...</i>
        :
        <>
          <h3 className='app-title'>
            {decodeURI(keyword)}
          </h3>
          <ListOfGifs gifs={gifs} />
        </>
    }
    <br />
    <button onClick={handleNextPage}>
      Get next page
    </button>
  </>
}
