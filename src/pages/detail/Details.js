import React, { useContext } from "react";
import Gif from "components/gif/Gif";
import GifsContext from 'context/GifsContext';

export default function Details({ params }) {
  const { gifs } = useContext(GifsContext)
  console.log(gifs);

  const gif = gifs.find(singleGif => singleGif.id === params.id);

  // console.log(params.id)

  return <Gif {...gif} />
};
