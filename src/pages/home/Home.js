import React, { useState } from "react";
import { useLocation } from "wouter";
import ListOfGifs from "components/ListOfGif/ListOfGifs";
import { useGifs } from "hooks/useGifs/useGifs";
import TrendingSearches from 'components/trendingSearches/index';

export default function Home() {
  const [keyword, setKeyword] = useState('');
  const [path, pushLocation] = useLocation();
  const { loading, gifs } = useGifs()

  const handleSubmit = evt => {
    evt.preventDefault()
    //navegar oa otra ruta
    pushLocation(`search/${keyword}`)
    console.log(keyword)
  }

  const handleChange = evt => {
    setKeyword(evt.target.value)

  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Search a gif here..."
          onChange={handleChange}
          value={keyword} />
        <button>
          Search
        </button>
      </form>
      <div className="App-main" >
        <div className="App-results" >
          <h3 className="App-title">
            Ultima busqueda
          </h3>
        </div>
        <ListOfGifs gifs={gifs} />
        <div className="App-category">
          <TrendingSearches />
        </div>
      </div>
    </>
  )
}
