import React from "react";
import './App.css';
import Home from "./pages/home/Home";
import SearchResults from './pages/searchResults/SearchResults';
import Details from './pages/detail/Details';
import { Link, Route } from 'wouter';
import StaticContext from './context/StaticContext';
import { GifsContextProvider } from './context/GifsContext';

export default function App() {

  return (
    <StaticContext.Provider value={
      {
        name: 'midudev',
        suscribete: true
      }
    }>
      <div className="App">
        <section className="App-content">
          <h1>App</h1>
          <Link to='/'>
            Index
          </Link>
          <GifsContextProvider>
            <Route
              component={Home}
              path='/'
            />
            <Route
              component={SearchResults}
              path='/search/:keyword'
            />
            <Route
              component={Details}
              path='/gif/:id'
            />
          </GifsContextProvider>
        </section>
      </div>
    </StaticContext.Provider>
  );
};
